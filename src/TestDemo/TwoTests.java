package TestDemo;

import org.testng.annotations.Test;

import com.perfectomobile.httpclient.MediaType;
import com.perfectomobile.httpclient.utils.FileUtils;
import com.perfectomobile.selenium.MobileDriver;
import com.perfectomobile.selenium.api.IMobileDevice;
import com.perfectomobile.selenium.api.IMobileDriver;
import com.perfectomobile.selenium.api.IMobileWebDriver;
import com.perfectomobile.selenium.options.MobileBrowserType;
import com.perfectomobile.selenium.options.MobileDeviceFindOptions;
import com.perfectomobile.selenium.options.MobileDeviceOS;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class TwoTests {
	
	private static final String siteUnderTest = "http://nxc.co.il/demoaut/index.php";
	private static ConcurrentHashMap<MobileDriver,IMobileDevice> DeviceMap = new ConcurrentHashMap<>();
	private static List<WebDriver> desktopDriverList = new ArrayList<>();
	
	public MobileDriver getNewMD(){
        return new MobileDriver("partners.perfectomobile.com","mitchellhwright@gmail.com","Demo1234");
	}
	
	public void LoginToSUT(Object driver) {
		WebElement nameElement = null, passwordElement = null;
		
		if(driver instanceof IMobileWebDriver){
			IMobileWebDriver imwd = (IMobileWebDriver)driver;
			nameElement = imwd.findElement(By.name("username"));
			passwordElement = imwd.findElement(By.name("password"));
		}
		else if(driver instanceof WebDriver){
			WebDriver wd = (WebDriver)driver;
			nameElement = wd.findElement(By.name("username"));
			passwordElement = wd.findElement(By.name("password"));
		}
		
		nameElement.sendKeys("John");
		passwordElement.sendKeys("Perfecto1");
	}
	
	//Add to Object Array to test different browsers
	//Could easily use spreadsheet to populate the array
	@DataProvider(parallel = true)
	public Object[][] dp() {
		return new Object[][] {
			new Object[] { "IOS", getNewDevice(MobileDeviceOS.IOS) },
			new Object[] { "Desktop FF", new SimpleEntry<Object,IMobileDevice>(new FirefoxDriver(), null)},
			new Object[] { "Android", getNewDevice(MobileDeviceOS.ANDROID) },
			new Object[] { "BB", getNewDevice(MobileDeviceOS.BLACKBERRY) },
			new Object[] { "Windows Phone", getNewDevice(MobileDeviceOS.WINDOWS_PHONE) },
		};
	}
	
	@Test(dataProvider = "dp", threadPoolSize = 10, timeOut = 100000)
	public void TestWebSite(String n, SimpleEntry<Object,IMobileDevice> newDevice) {
		
		System.out.println("Run " + n + " started");

		if(newDevice.getKey() instanceof MobileDriver && newDevice.getValue() != null){
			TestCellPhoneBrowser((MobileDriver)newDevice.getKey(), newDevice.getValue());
		}
		else if (newDevice.getKey() instanceof WebDriver) {
			TestDesktopBrowser((WebDriver)newDevice.getKey());
		}
		else Assert.fail("Test " + n + " did not have correct driver/device specs!");
		
		System.out.println("Run " + n + " ended");
	}

	public void TestCellPhoneBrowser(MobileDriver driver, IMobileDevice device){
		
		DeviceMap.put(driver, device);

		IMobileWebDriver domDriver = device.getDOMDriver(MobileBrowserType.DEFAULT);

		domDriver.get(siteUnderTest);
		
		//Login to web application - set user and password
		LoginToSUT(domDriver);

		//click "Sign in" button
		WebElement textElement = domDriver.findElement(By.linkText("Sign in"));
		textElement.click();

		//Text checkpoint on "Welcome back John"
		IMobileWebDriver visualDriver = device.getVisualDriver();
		visualDriver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

		visualDriver.findElement(By.linkText("Welcome back John"));
	}

	public void TestDesktopBrowser(WebDriver driver){
		
		desktopDriverList.add(driver);
		
		driver.get(siteUnderTest);
	
		//Login to web application - set user and password
		LoginToSUT(driver);

		//click "Sign in" button
		WebElement textElement = driver.findElement(By.cssSelector(".login>div>button"));
		textElement.click();
		
		//Text checkpoint on "Welcome back John"
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.cssSelector("#welcome>h3")) != null;
            }
        });
	
		String welcomeText = driver.findElement(By.cssSelector("#welcome>h3")).getText();
		
		Assert.assertTrue(welcomeText.contains("Welcome back John"));
	}
	
	public SimpleEntry<Object,IMobileDevice> getNewDevice(MobileDeviceOS mdos){
		
		MobileDriver driver = getNewMD();
		
		MobileDeviceFindOptions options = new MobileDeviceFindOptions();
		options.setOS(mdos);
		
		IMobileDevice device = driver.findDevice(options);
		device.open();
		device.home();
				
		return new SimpleEntry<Object, IMobileDevice>(driver, device);
	}
	
	@AfterClass
	public void afterClass() {
		
		DeviceMap.entrySet().stream().forEach(x->{
		//Close the device
		x.getValue().close();
		//quit the driver
		x.getKey().quit();
		//download the report
		downloadReport(x.getKey());
		});
		
		desktopDriverList.stream().forEach(x -> x.quit());
	}
	
    /*
     * Download the report to a specified location.
     * This method should be used after calling driver.quit().
     */
    private static void downloadReport(IMobileDriver driver) {
        InputStream reportStream = driver.downloadReport(MediaType.PDF);
        if (reportStream != null) {
            String path = "C:\\eclipse-java-mars\\workspace\\MitchDemo\\report_"+System.currentTimeMillis() / 1000L +".pdf";
            File reportFile = new File(path);
            try {
                FileUtils.write(reportStream, reportFile);
            } catch (IOException e) {
                System.out.println("Failed to write report to path: " + path + " - " + e.getMessage());
            }
        }
    }
}
